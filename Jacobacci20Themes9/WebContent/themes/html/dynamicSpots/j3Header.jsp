<%@ page session="false" buffer="none" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="../includePortalTaglibs.jspf" %>
<fmt:setBundle basename="com.jacobacci.portal.nl.extra20" />
<c:set var="locale" value="<%= request.getLocale() %>" />
<portal-core:constants/><portal-core:defineObjects/>

  <div id="header">
	<portal-logic:if loggedIn="no" >
		<div id="logo"><a href="/wps/portal/jep20"><img src="/Jacobacci20Themes/themes/extra20/frontend/images/logo.gif" alt="J3 - Jacobacci Extranet" border="0" /></a></div>
    </portal-logic:if>
	<portal-logic:if loggedIn="yes" >
		<div id="logo"><a href="/wps/myportal/jep20"><img src="/Jacobacci20Themes/themes/extra20/frontend/images/logo.gif" alt="J3 - Jacobacci Extranet" border="0" /></a></div>
    </portal-logic:if>
    <portal-logic:if loggedIn="true">
	    <div id="mini_tools"><fmt:message key="header.welcomemsg" ><fmt:param><strong><portal-fmt:user attribute="givenName"/> <portal-fmt:user attribute="sn"/></strong></fmt:param></fmt:message> |
	    <portal-logic:if portletSolo="no">
           <portal-navigation:urlGeneration 
           		contentNode="com.jacobacci.SelfCare" 
           		layoutNode="com.jacobacci.p.SelfCare" 
           		portletWindowState="Normal"
           		portletMode="edit" 
           		themeTemplate="Extra20ThinSkin">
                   <portal-navigation:urlParam type="render" name="ao" value="thm"/>
                   <portal-navigation:urlParam type="action" name="action" value="wps.portlets.edit_profile" />
                   <a href='<% wpsURL.write(escapeXmlWriter); %>' ><fmt:message key="header.editprofile" /></a> | 
           </portal-navigation:urlGeneration>
		</portal-logic:if>
		<portal-logic:if portletSolo="no">
		    <a href="<portal-navigation:url command="LogoutUser" />"><fmt:message key="header.logout" /></a>
	    </portal-logic:if>
	    <%-- <br />
	    <a href="<portal-navigation:url command="ChangeLanguage" ><portal-navigation:urlParam name="locale" value="it"/></portal-navigation:url>">Italiano</a> - 
	    <a href="<portal-navigation:url command="ChangeLanguage" ><portal-navigation:urlParam name="locale" value="en"/></portal-navigation:url>">English</a> - 
	    <a href="<portal-navigation:url command="ChangeLanguage"><portal-navigation:urlParam name="locale" value="fr"/></portal-navigation:url>">Fran�ais</a>
	    --%>
	    </div>
    </portal-logic:if>
  </div>