<%@ page session="false" buffer="none" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="../includePortalTaglibs.jspf" %>
<portal-core:constants/><portal-core:defineObjects/>

  <div id="footer">
  	<a href="http://www.jacobacci.com" target="_blank" style="display: block; width: 120px; height: 50px;float:left;" ></a>
  	<a style="float:right;margin-right:15px;margin-top:15px;" target="_blank" href=
  		<c:choose>
  			<c:when test="${locale == 'it' }">
  				<c:out value="https://www.iubenda.com/privacy-policy/118042/full-legal"/>
  			</c:when>
  			<c:otherwise>
  				<c:out value="https://www.iubenda.com/privacy-policy/787622/full-legal"/>
  			</c:otherwise>
  		</c:choose>
  		>
  		Privacy & cookies</a>
  </div>