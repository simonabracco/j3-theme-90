<%@page import="com.ibm.portal.navigation.NavigationNode"%>
<%@page import="java.util.Iterator"%>
<%@ page session="false" buffer="none" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="../includePortalTaglibs.jspf" %>
<portal-core:constants/><portal-core:defineObjects/>

  <portal-logic:pageMetaData varname="pageMetaData">
  	<c:set var="nonavigation" value='<%= String.valueOf("true".equals(pageMetaData.getValue("no-navigation"))) %>'></c:set>
  </portal-logic:pageMetaData>
  <c:if test="${nonavigation == true}">
  	<div id="top_main"></div>
  </c:if>
  <c:if test="${nonavigation == false}">
  <portal-logic:pageMetaData varname="pageMetaData">
  	<c:set var="rootNode" value='<%=pageMetaData.getValue("rootNode")%>'></c:set>
  </portal-logic:pageMetaData>
	  <portal-navigation:navigation  scopeUniqueName='<%=(String) pageContext.getAttribute("rootNode") %>' stopLevel="1">
	  	  <c:set var="counter" value="1" />
		  <div id="menu-nav">
		  <!-- Navigation first level -->
		    <ul id="nav">
		  	<portal-navigation:navigationLoop>
		  		<%
		  			boolean isNodeVisible = true;
		  			String j3role = ""; 
		  			if (wpsNavNode instanceof com.ibm.portal.MetaDataProvider) { 
		  				com.ibm.portal.MetaData iMetaData=((com.ibm.portal.MetaDataProvider) wpsNavNode).getMetaData();
		  				Object hiddenValue = iMetaData.getValue("com.ibm.portal.Hidden");
						if (hiddenValue == null) { hiddenValue = iMetaData.getValue("com.jacobacci.portal.Hidden"); }
						if(hiddenValue != null){
							if(hiddenValue.toString().equalsIgnoreCase("true")){
								isNodeVisible=false;
			  				} else {
			  					isNodeVisible=true;
		  					}
		  				}
						
		  				Object j3roleValue = iMetaData.getValue("com.jacobacci.portal.J3Role");
		  				if (j3roleValue != null) { j3role = j3roleValue.toString(); }
		  			}
		  			pageContext.setAttribute("j3role", j3role);
		  			pageContext.setAttribute("isJ3Role", String.valueOf(!j3role.equals("")));
		  			
		  			request.setAttribute("isNodeVisible",String.valueOf(isNodeVisible));
		  			boolean isNodeSelected = wpsSelectionModel.isNodeSelected(wpsNavNode);
					String rowCssClass = isNodeSelected ? "wptheme-sideNavItem wptheme-sideNavSelected" : "wptheme-sideNavItem";
					boolean nodeHasChildren = wpsNavModel.hasChildren(wpsNavNode);
					boolean isExpanded = ((Boolean)((com.ibm.portal.state.StateModel)wpsNavModel).getState(wpsNavNode, com.ibm.portal.state.StateType.EXPANSION)).booleanValue() ;
       	 			boolean openInNewWindow = com.ibm.portal.content.ContentNodeType.EXTERNALURL.equals(wpsNavNode.getContentNode().getContentNodeType());
        			boolean isLabel = com.ibm.portal.content.ContentNodeType.LABEL.equals(wpsNavNode.getContentNode().getContentNodeType());
					int currentNavLevel = wpsNavLevel.intValue();
		  		%>
		  		<c:if test="${requestScope.isNodeVisible}">	  	
			  		<c:set var="isSelected" value="<%= String.valueOf(wpsSelectionModel.isNodeSelected(wpsNavNode) || wpsSelectionModel.isNodeInSelectionPath(wpsNavNode)) %>" />
			  		<c:choose>
			  			<c:when test="${counter == 1 && isSelected}"><c:set var="itemClass" value="first_white" /></c:when>
			  			<c:when test="${counter == 1 && ! isSelected}"><c:set var="itemClass" value="first_blue" /></c:when>
			  			<c:when test="${counter >= 1}"><c:set var="itemClass" value="" /></c:when>
			  		</c:choose>
			  		<li class='${isSelected ? "top_active" : "top"} ${itemClass} ${isJ3Role ? "j3role" : ""}' data-j3role="${j3role}">
			  			<span>
			  				<a href="<portal-navigation:navigationUrl  type="link" />" 
			  					title="<portal-fmt:description varname="<%=wpsNavNode%>"/>">
			  					<portal-fmt:title varname="<%=wpsNavNode%>"/>
			  				</a>
			  			</span>
			  			
			  			<!-- Navigation second level -->
			  			<%
			  				Iterator children =  wpsNavModel.getChildren(wpsNavNode);
			  				if(children.hasNext()){
			  			%>
			  			<ul>
				  			<%
				  				while(children.hasNext()){ 
				  				NavigationNode node = (NavigationNode) children.next();
				  			%>
				  				<li>
				  					<a href='<portal-navigation:navigationUrl  type="link" varname="<%=node%>"/>'
        								title='<portal-fmt:description varname="<%=node%>"/>'>
        								<portal-fmt:title varname="<%=node%>"/>
     								</a>
     							</li>
					  		<%} %>
			  			</ul>
			  			<%} %>
			  		</li>
			  		<c:set var="counter" value="${counter + 1}" />
			  	</c:if>
		  	</portal-navigation:navigationLoop>
		    </ul>
		  </div>	  	
	  </portal-navigation:navigation>
  </c:if>