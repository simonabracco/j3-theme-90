$(document).ready(function()
{
	
	var what;
	$('ul#nav > li > span > a').tooltip({ tipClass: "tooltipMenuTop" });

	$('li').click(function(e)
	{
		e.stopPropagation();
	});
	
	var uls = $("ul#nav  li  ul");
	
	for(var i = 0;i<uls.length;i++){
		var ul = uls[i];
		var parentLi=$(ul).parent("li");
		var difference = 0;
		if($(parentLi).hasClass("top_active")){
			difference = 10;
		}
		var width = ($(parentLi).width()-difference);
		$(ul).width(width);
	}
	
	/* cookie law modal dialog not possible: cross site scripting issue...
	var cookielaw_url = $('a.cookiewlaw:first').attr('href');
	var $modalDialog = $('<div/>', { 'class': 'exampleModal', 'id': 'exampleModal1' })
    .appendTo('body')
    .dialog({
        resizable: false,
        autoOpen: false,
        height: 300,
        width: 350,
        show: 'fold',
        buttons: {
            "Fire Event": function () {
                alert("Event Fired");
            }
        },
        modal: true
    });
	
	$('a.cookiewlaw').on('click', function (e) {
        e.preventDefault();
        $modalDialog.load(url);
        $modalDialog.dialog("open");
    });
	*/
});

openbox = function(what) {	
	
	if ($("#pbody_"+what).is(":hidden")) {
		$("#arrow_"+what).css("backgroundPosition","0 0");
		$("#pbody_"+what).slideDown("fast");
    } else {
		$("#arrow_"+what).css("backgroundPosition","0 -11px");
      	$("#pbody_"+what).slideUp("fast");
    }

};

opensubbox = function(what) {	
	
	if ($("div#"+what).is(":hidden")) {
		$("div#arrow_"+what).css("backgroundPosition","0 -11px");
		$("div#"+what).slideDown("fast");
    } else {
		$("div#arrow_"+what).css("backgroundPosition","0 0");
      	$("div#"+what).slideUp("fast");
    }

};

addNewRelicCustomAttribute = function (attributeName, attributeValue){
	if (typeof(newrelic) !== 'undefined'){
		newrelic.setCustomAttribute(attributeName, attributeValue);
	}
};

